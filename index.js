
let pokemons = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur",]
let friends2 = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
}

let Trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: (pokemons),
    friends: (friends2),
    talk: function(){
        console.log("Result of talk method");
    }
}


console.log(Trainer);

console.log("Result from dot notation: ");
console.log(Trainer.name);

console.log("Result from square bracket notation:")
console.log(Trainer["pokemon"]);

Trainer.talk()
console.log("Pikachu! I choose you!")


function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = level*2;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log("Pikachu's health is now reduced to " + target.health)

        if(target.health <=0){
            target.faint()
        }
    }

    this.faint = function(){
        console.log(this.name + " fainted");
    }

}

let pikachu = new Pokemon ("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon ("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon ("Mewtwo", 100);
console.log(mewtwo);


geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

console.log(geodude);